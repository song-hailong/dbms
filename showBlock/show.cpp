#include "show.h"
void showDatabase(dbBlock* dbs);//展示数据库
void showTable(tbBlock* tbs);//展示数据表
void showMsg(dataBlock* dts);//展示表的数据
/*
 *展示数据库模块 
 *功能：展示当前有多少数据库
 */
void showDatabase(dbBlock* dbs)
{
    if(dbs==NULL)
    {
        cout<<"none database"<<endl;
        return ;
    } 
   
    while(dbs!=NULL)
    {
        // cout << "--------------------------------------" << endl;
        cout<<dbs->name<<endl;
        cout<<"tables:"<<endl;
        if(dbs->tbPointer!=NULL){
            showTable(dbs->tbPointer);
        }
        dbs=dbs->next;
        // cout << "--------------------------------------" << endl;
        cout << endl;
        cout << ">>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<" << endl;
        cout << endl;
    }
}

/*
 *展示数据表模块 
 *功能：展示有多少数据表
 */
void showTable(tbBlock* tbs)
{
    if(tbs==NULL)
    {
        cout<<"none tables"<<endl;
        return ;
    }
    while(tbs!=NULL)
    {
        cout<<"     "<<tbs->name<<endl;

        if(tbs->dataPointer != NULL ){
            cout<<"         ";
            Abbr *t = tbs->abbr;
            while(t != NULL){
                cout<<t->name<<"  ";
                t = t->next;
            }
            cout<<endl;
            showMsg(tbs->dataPointer);
        }
        tbs=tbs->next;
    }
}

/*
 *展示表中数据模块 
 *功能：展示表里的数据
 */
void showMsg(dataBlock* dts)
{
    if(dts == NULL){
        cout<<"none datas"<<endl;
        return ;
    }
    while(dts != NULL){
        cout<<"         ";
        for(int i=0;i<dts->n;i++){
            cout<<dts->value[i]<<"      ";
        }
        cout<<endl;
        dts = dts->next;
    }
    cout<<endl;
}
