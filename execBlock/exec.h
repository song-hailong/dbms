#ifndef EXEC_H
#define EXEC_H

#include <iostream>
#include <string>
#include <direct.h>

#include "../showBlock/show.h"
#include "../global.h"
#include "../fileBlock/fileManager.h"
#include "exec.cpp"

void doMain();//主要流程

void doAct();//操作选择

bool addMsg();//添加信息

bool delMsg();//删除信息

bool searchMsg();//查找信息

bool writeMsg();//修改信息

#endif