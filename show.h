#ifndef SHOW_H
#define SHOW_H
#include<iostream>
#include "../global.h"
#include "show.cpp"
void showDatabase(dbBlock* dbs);//展示数据库
void showTable(tbBlock* tbs);//展示数据表
void showMsg(dataBlock* dts);//展示表的数据
#endif