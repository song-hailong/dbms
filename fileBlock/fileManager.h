#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <iostream>
#include <string>
#include <fstream>
#include <io.h>
#include <string.h>
#include <stdlib.h>
#include "../global.h"
#include "fileManager.cpp"
using namespace std;

void loadDB();   //获取当前数据库文件信息
void updateDB(); //写入数据
#endif