#include "fileManager.h"
/*
 *读取数据库信息模块 
 *功能：获得数据库信息写入变量中
 */
void loadDB()
{
	dbPointer = NULL;
    dbPointer = new dbBlock();
    dbBlock *t = dbPointer;

    //文件信息 
	struct _finddata_t fileinfo;
	string strP;

    //文件句柄
	long long  hFile = 0;

	//相对路径转绝对路径
	char fullpath[4096] = "./dbFile";
	if ((hFile = _findfirst(strP.assign(fullpath).append("/*").c_str(), &fileinfo)) != -1)
	{
		long long tmp_t;
		do
		{   /*_A_SUBDIR 判断为目录*/
			if ((fileinfo.attrib & _A_SUBDIR))
			{
				/*匹配到目录--此处为数据库名*/
				if ( strcmp(fileinfo.name, ".") == 0 || strcmp(fileinfo.name, "..") == 0 )
				{
					tmp_t = _findnext(hFile, &fileinfo);
					continue;
				} 
                t->name = new char[sizeof(fileinfo.name)+1];
                strcpy(t->name,fileinfo.name);//将文件夹名字复制到变量中
				
				/*对目录下txt文件操作--数据表文件*/
				string strP_t = "./dbFile/";
				strP_t=strP_t.append(t->name).append("/*.txt");
				struct _finddata_t fileinfo_t;
				long long hFile_t;

				if((hFile_t = _findfirst(strP_t.c_str(),&fileinfo_t))!=-1)//寻找txt文件
				{	
					t->n = 0;//表计数
					t->tbPointer = new tbBlock();
					tbBlock *table_t = t->tbPointer;
					long long tmp_h; 
					do
					{	
						t->n++;//表个数+1
						/*找到了表格txt文件*/
						table_t->name = new char[sizeof(fileinfo_t.name)+1];
						strcpy(table_t->name,fileinfo_t.name);
							
						/*读取txt文件*/
						/*.......code......*/
						{
							ifstream infile;
							string txtP = "./dbFile/";
							
							infile.open((txtP.append(t->name).append("/").append(table_t->name)).c_str(),ios::in);
							/*文件打开失败*/
							if(!infile.is_open())
							{
								cout<<"read error"<<endl;
							}

							char buf[1024];//txt一行的值
							int tmp=0;
							infile.getline(buf,sizeof(buf));
							{
								/*添加标签*/
								if(tmp == 0)
								{
									table_t->abbr_n=0;
									table_t->abbr = new Abbr();
									Abbr *abbr_t = table_t->abbr;
									/*分割字符串*/
									char *value = strtok(buf," ");//第一个标签
									while(value != NULL)
									{
										table_t->abbr_n++;//记录标签数
										abbr_t->name = new char[sizeof(value)+1];
										strcpy(abbr_t->name,value);
											
										value = strtok(NULL," ");//继续获得子串
										if(value != NULL)
										{ 
											abbr_t->next = new Abbr();
											abbr_t = abbr_t->next;//移至下一个标签		
										}
									}
									tmp++;
								}
							}
							/*添加数据*/
							table_t->dataPointer = new dataBlock();
							dataBlock *data_t = table_t->dataPointer;
							while(infile.getline(buf,sizeof(buf)))
							{
								/*初始化数据行*/
								data_t->n = table_t->abbr_n;
								data_t->value = new int [data_t->n];
								int num_t = 0; 
								char *value = strtok(buf," ");	
								while(value != NULL)
								{
									data_t->value[num_t++] = atoi(value);
									//cout<<data_t->value[num_t-1];
									value = strtok(NULL," ");
								}
								data_t->next = new dataBlock();
								data_t = data_t->next;
							}
						}
						/*判断是否有下一个txt文件，有就增加子节点*/
						tmp_h = _findnext(hFile_t,&fileinfo_t);//判断还有没有txt文件
						if(tmp_h==0) table_t->next = new tbBlock();
						table_t = table_t->next;//寻找下一个表格
					}while(tmp_h == 0);
				}
				_findclose(hFile_t);

				/*判断是否有下一个文件夹，有就增加子节点*/
				tmp_t = _findnext(hFile, &fileinfo);
				if(tmp_t == 0)	t->next = new dbBlock();
				t = t->next;
			}	
		} while ( tmp_t == 0);
		_findclose(hFile);
	}		
}

/*
 *更新数据库信息模块 
 *更新当前数据库信息
 */
void updateDB(){
    
}
