#ifndef GLOBAL_H
#define GLOBAL_H

#include<iostream>
using namespace std;

/*数据标签*/
struct Abbr{
    char* name;//标签名
    Abbr* next;//下一列标签
};

/*数据块--表中一行信息*/
struct dataBlock{
    int n;//数据个数
    dataBlock *next;//指向下一行
    int *value;//一行的值
};

/*数据表块*/
struct tbBlock{
    char* name;
    int abbr_n;//标签数
    Abbr* abbr;//数据标签
    tbBlock *next;//下一个表
    dataBlock* dataPointer;//指向数据表的多个数据块--表中多行信息
};

/*数据库块*/
struct dbBlock{
    char* name;
    int n;//数据表个数
    dbBlock* next;//下一个数据库
    tbBlock* tbPointer;//指向多个数据表块
};

dbBlock* dbPointer;//指向多个数据库块
int countDB;//数据库块个数

#endif